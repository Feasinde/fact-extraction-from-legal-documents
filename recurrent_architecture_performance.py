import argparse
import os
from typing import Any, List

import pandas
import spacy
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from main.modules.pytorch_utils import get_device
from main.modules.recurrent_architecture_modules import LSTMLinear, GRULinear, AttentionEncoderDecoder
from main.pipelines.steps.neural_training_mixin import NeuralTrainingMixin
from main.pipelines.steps.recurrent_architectures_steps.binarise_documents import DocumentDataset

DEVICE = get_device()
HOME_FOLDER = os.path.expanduser("~")
DATA_FOLDER = "{}/PycharmProjects/fact-extraction-from-legal-documents/data".format(HOME_FOLDER)


def predict(model: torch.nn.Module,
            test_data: str,
            tokeniser: Any,):
    test_set = DocumentDataset(ds_loc=test_data, annotated=True, max_len=256)
    test_set = DataLoader(dataset=test_set, batch_size=1)
    model.to(DEVICE)
    with torch.no_grad():
        pred = []
        targets = []
        for doc in tqdm(test_set, desc="Binarising documents"):
            input, annotations = doc
            pred.append(model(input.to(DEVICE)))
            targets.append(annotations.squeeze(0))
        pred = torch.cat(pred)
        targets = torch.cat(targets)
    return pred, targets


parser = argparse.ArgumentParser()
parser.add_argument("--arq", type=str)
parser.add_argument("--model", type=str)
parser.add_argument("--dataset", type=str)
args = parser.parse_args()

model_inv = {
    "lstm" : LSTMLinear(embedding_size=768),
    "gru" : GRULinear(embedding_size=768),
    "attn" : AttentionEncoderDecoder(hidden_size=768, max_len=128, device=get_device())
}

model = model_inv[args.arq]
model.load_state_dict(torch.load(args.model))
model.eval()

tokeniser = spacy.load("fr_core_news_sm")
output = predict(model=model, test_data=args.dataset, tokeniser=tokeniser)
augm = args.dataset[-1]
pred, targets = output
print(pred.shape, targets.shape)
print("MODEL AUGMENT: {}".format(augm))
acc, pre, rec, f1 = NeuralTrainingMixin._test(targets, pred)
print("acc: {}".format(acc))
print("pre: {}".format(pre))
print("rec: {}".format(rec))
print("f1: {}".format(f1))
