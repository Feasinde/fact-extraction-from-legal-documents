import argparse
import os

import gensim
import pandas
import spacy
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from main.modules.pytorch_utils import get_device
from main.modules.sentence_classifier_module import SentenceClassifier
from main.pipelines.steps.neural_training_mixin import NeuralTrainingMixin
from main.pipelines.steps.sentence_classifier_steps.classify_sentences import SentenceDataset, PadSequence
from main.pipelines.steps.sentence_classifier_steps.get_sentences import GetSentences
from main.pipelines.steps.sentence_classifier_steps.vectorise_sentences import VectoriseSentences

DEVICE = get_device()
HOME_FOLDER = os.path.expanduser("~")


def predict(model: torch.nn.Module,
               test_data: pandas.DataFrame,
               tokeniser,
               emb_model: gensim.models.keyedvectors.Word2VecKeyedVectors):
    model.eval().to(DEVICE)
    get_sentences_step = GetSentences(tokeniser=tokeniser)
    (sentences, annotations), _ = get_sentences_step.transform(df=test_data)
    vectorise_sentences_step = VectoriseSentences(emb_model=emb_model, tokeniser=tokeniser)
    sentences = vectorise_sentences_step.transform(sentences)
    test_set = SentenceDataset(docs=sentences, annotations=annotations)
    test_set = DataLoader(dataset=test_set, batch_size=512, collate_fn=PadSequence())
    with torch.no_grad():
        pred = []
        targets = []
        for batch in tqdm(test_set, desc="Classifying sentences"):
            sents, annotation, lengths = batch
            pred_batch = model(sents.to(DEVICE), lengths)
            pred += pred_batch
            targets += annotation.squeeze()
        pred = torch.stack(pred)
        torch.round_(pred)
        targets = torch.stack((targets))
    return pred, targets


parser = argparse.ArgumentParser()
parser.add_argument("--model", type=str)
parser.add_argument("--dataset", type=str)
args = parser.parse_args()

emb_model = gensim.models.KeyedVectors.load_word2vec_format(
    "{}/PycharmProjects/fact-extraction-from-legal-documents/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin".format(HOME_FOLDER),
    binary=True,
    unicode_errors='ignore'
)
model = SentenceClassifier(embeddings_tensor=torch.tensor(emb_model.vectors))
model.load_state_dict(torch.load(args.model))
tokeniser = spacy.load("fr_core_news_sm")
dataframe = pandas.read_csv(f"{args.dataset}", sep="\t")
pred, targets = predict(model=model, test_data=dataframe, tokeniser=tokeniser, emb_model=emb_model)
acc, pre, rec, f1 = NeuralTrainingMixin._test(targets, pred)
print("acc: {}".format(acc))
print("pre: {}".format(pre))
print("rec: {}".format(rec))
print("f1: {}".format(f1))
