import os

import pandas
import spacy

from main.pipelines.steps.recurrent_architectures_steps.augment_documents import AugmentDocuments
from main.pipelines.steps.recurrent_architectures_steps.create_camembert_representations import \
    CreateCamembertRepresentations
from main.pipelines.steps.recurrent_architectures_steps.get_documents import GetDocuments

training_set = "data/training_data_20210318"
test_set = "data/test_data_20210318"
tokeniser = spacy.load("fr_core_news_sm")
augment_list = [0, 1, 2]
datasets = [training_set, test_set]


def create_dataset(df_string: str, augment: int = 0, training=True):
    df = pandas.read_csv(df_string, sep="\t")
    get_docs_step = GetDocuments(tokeniser=tokeniser)
    documents, splitting_indices = get_docs_step.transform(data_inputs=df)
    augment_docs = AugmentDocuments(augment=augment,  tokeniser=tokeniser)
    _, (documents, splitting_indices) = augment_docs.fit_transform(data_inputs=documents, expected_outputs=splitting_indices)
    create_reps_step = CreateCamembertRepresentations(tokeniser=tokeniser)
    create_reps_step = create_reps_step.fit(data_inputs=None, expected_outputs=splitting_indices)
    fraction = f"training_augm_{augment}" if training else "test"
    create_reps_step.transform(data_inputs=documents, data_loc=f"cb_reps_{fraction}")


create_dataset(test_set, training=False)
