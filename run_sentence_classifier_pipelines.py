import argparse

import gensim
import pandas
import spacy
import torch
# TODO separate original sents from augmented sents for validation
from main.pipelines.sentence_classifier_pipelines import GetSentencesPipeline, AugmentSentencesPipeline, \
    SentenceClassifierPipeline, CreateGoldStandardPipeline, GetOffsetsPipeline, SplitSentencesPipeline

parser = argparse.ArgumentParser()

parser.add_argument("--dataset", type=str)
parser.add_argument("--augment", type=int)
args = parser.parse_args()

tokeniser = spacy.load("fr_core_news_sm")
emb_model = gensim.models.KeyedVectors.load_word2vec_format(
    "frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore'
)
df = pandas.read_csv(args.dataset, sep="\t")
augment_range = [args.augment]

get_sentences_pipeline = GetSentencesPipeline(tokeniser=tokeniser)

(sents, annotations), splitting_indices = get_sentences_pipeline.transform(data_inputs=df)

for augment in augment_range:
    print("AUGMENT = {}".format(augment))
    augment_sentences_pipeline = AugmentSentencesPipeline(tokeniser=tokeniser, augment=augment)
    sentence_classifier_pipeline = SentenceClassifierPipeline(tokeniser=tokeniser, emb_model=emb_model, augment=augment)
    _, (sents, annotations) = augment_sentences_pipeline.fit_transform(data_inputs=sents, expected_outputs=annotations)
    _, prediction = sentence_classifier_pipeline.fit_transform(data_inputs=sents, expected_outputs=annotations)
    model = sentence_classifier_pipeline.steps['classify_sentences'].model
    model.to(torch.device("cpu"))
    torch.save(model.state_dict(), f"model_sent_{augment}")
