import argparse

import gensim
import pandas
import spacy
import torch

from main.modules.pytorch_utils import get_device
from main.modules.recurrent_architecture_modules import GRULinear, LSTMLinear, AttentionEncoderDecoder
from main.modules.sentence_classifier_module import SentenceClassifier
from main.pipelines.steps.create_goldstandard import CreateGoldStandard
from main.pipelines.steps.get_offsets import GetOffsets
from main.pipelines.steps.recurrent_architectures_steps.get_documents import GetDocuments
from main.pipelines.steps.sentence_classifier_steps.get_sentences import GetSentences
from main.pipelines.steps.split_sentences import SplitSentences, SplitDocuments

parser = argparse.ArgumentParser()

parser.add_argument("--arch", type=str, help="Architecture to be used for splitting documents")
parser.add_argument("--model", type=str, help="State dictionary to load")
parser.add_argument("--dataset", type=str, help="Test dataset to evaluate splitting")
parser.add_argument("--datafolder", type=str, help="Folder containing cb representations")

args = parser.parse_args()

if args.arch == "sent":
    emb_model = gensim.models.KeyedVectors.load_word2vec_format(
        "frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
        binary=True,
        unicode_errors='ignore')
    embeddings_tensor = torch.tensor(emb_model.vectors)
else:
    emb_model = None
    embeddings_tensor = None

model_inv = {
    "lstm" : LSTMLinear(embedding_size=768),
    "gru" : GRULinear(embedding_size=768),
    "attn" : AttentionEncoderDecoder(hidden_size=768, max_len=128, device=get_device()),
}

gamma_range = [0.6, 0.8, 1, 1.2, 1.4]

tokeniser = spacy.load("fr_core_news_sm")
df = pandas.read_csv(args.dataset, sep="\t")

if args.arch == "sent":
    model = SentenceClassifier(embeddings_tensor=embeddings_tensor)
else:
    model = model_inv[args.arch]
model.load_state_dict(torch.load(args.model))
create_gold_standard = CreateGoldStandard()
documents = create_gold_standard.transform(df=df)
get_offsets = GetOffsets()
if args.arch == "sent":
    get_sentences = GetSentences(tokeniser=tokeniser)
    splitter = SplitSentences(model=model, tokeniser=tokeniser, emb_model=emb_model, gamma=gamma_range)
    (sentences, _), splitting_indices = get_sentences.transform(df=df)
    pred_indices = splitter.transform(documents)

else:
    get_documents = GetDocuments(tokeniser=tokeniser)
    _, splitting_indices = get_documents.transform(data_inputs=df)
    splitter = SplitDocuments(model=model, gamma=gamma_range, tokeniser=tokeniser)
    pred_indices = splitter.transform(data_inputs=args.datafolder)

offset_list = get_offsets.transform(data_inputs=(pred_indices, splitting_indices))
for i, gamma in enumerate(gamma_range):
    offsets = offset_list[i]
    offsets = [str(value)+" & " for value in offsets.values()]
    print("GAMMA = {} ".format(gamma)+str(offsets)+"\n")




