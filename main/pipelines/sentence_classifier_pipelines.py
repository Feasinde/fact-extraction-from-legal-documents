from neuraxle.hyperparams.space import HyperparameterSamples
from neuraxle.pipeline import Pipeline

from main.pipelines.steps.create_goldstandard import CreateGoldStandard
from main.pipelines.steps.sentence_classifier_steps.augment_sentences import AugmentSentences
from main.pipelines.steps.sentence_classifier_steps.classify_sentences import ClassifySentences
from main.pipelines.steps.get_offsets import GetOffsets
from main.pipelines.steps.sentence_classifier_steps.get_sentences import GetSentences
from main.pipelines.steps.split_sentences import SplitSentences
from main.pipelines.steps.sentence_classifier_steps.vectorise_sentences import VectoriseSentences


class GetSentencesPipeline(Pipeline):

    def __init__(self, tokeniser):
        super().__init__([
            ('get_sentences', GetSentences(tokeniser=tokeniser)),
        ])


class AugmentSentencesPipeline(Pipeline):

    def __init__(self, tokeniser, augment=2):
        super().__init__([
            ('augment_sentences', AugmentSentences(augment=augment, tokeniser=tokeniser)),
        ])


class SentenceClassifierPipeline(Pipeline):

    DEFAULT_PARAMS = HyperparameterSamples({
        'classify_sentences__batch_size': 512,
        'classify_sentences__n_epochs': 25,
        'classify_sentences__learning_rate': 1e-3,
        'classify_sentences__weight_decay': 1e-4,
        'classify_sentences__clip': 0.2,
        'classify_sentences__device': 'cpu',
        'classify_sentences__lr_step': [10, 13],
        'classify_sentences__lr_gamma': 0.1,
    })

    def __init__(self, tokeniser, emb_model, augment, **hparams):
        super().__init__([
            ('vectorise_sentences', VectoriseSentences(emb_model=emb_model, tokeniser=tokeniser)),
            ('classify_sentences', ClassifySentences(emb_model=emb_model, tokeniser=tokeniser, augment=augment))
        ])

        for key, value in hparams.items():
            self.DEFAULT_PARAMS[key] = value
        self.set_hyperparams(self.DEFAULT_PARAMS)


class SplitSentencesPipeline(Pipeline):

    def __init__(self, model, tokeniser, emb_model, gamma):
        super().__init__([
            ('split_sentences', SplitSentences(model=model, tokeniser=tokeniser, emb_model=emb_model, gamma=gamma))
        ])


class GetOffsetsPipeline(Pipeline):

    def __init__(self):
        super().__init__([
            ('get_offsets', GetOffsets())
        ])

class CreateGoldStandardPipeline(Pipeline):

    def __init__(self):
        super().__init__([
            ('create_goldstandard', CreateGoldStandard())
        ])