# TODO write rec architecture
from typing import List, Any

import torch
from neuraxle.pipeline import Pipeline

from main.pipelines.steps.recurrent_architectures_steps.augment_documents import AugmentDocuments
from main.pipelines.steps.recurrent_architectures_steps.binarise_documents import BinariseDocuments
from main.pipelines.steps.recurrent_architectures_steps.create_camembert_representations import \
    CreateCamembertRepresentations
from main.pipelines.steps.recurrent_architectures_steps.get_documents import GetDocuments
from main.pipelines.steps.split_sentences import SplitDocuments


class GetDocumentsPipeline(Pipeline):

    def __init__(self, tokeniser):
        super().__init__([
            ('get_documents', GetDocuments(tokeniser=tokeniser))
        ])


class AugmentDocumentsPipeline(Pipeline):

    def __init__(self, augment: int, tokeniser):
        super().__init__([
            ('augment_documents', AugmentDocuments(augment=augment, tokeniser=tokeniser)),
        ])


class CreateCamembertRepresentationsPipeline(Pipeline):

    def __init__(self, tokeniser):
        super().__init__([
            ('create_camembert_representations', CreateCamembertRepresentations(tokeniser=tokeniser))
        ])


class RecurrentArchitecturePipeline(Pipeline):

    def __init__(self, model, augment, model_arq, max_len):
        super().__init__([
            ('recurrent_architecture', BinariseDocuments(model=model, augment=augment, model_arq=model_arq, max_len=max_len))
        ])


class SplitDocumentsPipeline(Pipeline):

    def __init__(self, model: torch.nn.Module, gamma: List[float], tokeniser: Any):
        super(SplitDocumentsPipeline, self).__init__([
            ('split_documents', SplitDocuments(model=model, gamma=gamma, tokeniser=tokeniser))
        ])