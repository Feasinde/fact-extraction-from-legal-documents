from random import choice
from typing import List

import nltk
from neuraxle.base import BaseStep, _FittableStep
from spacy.tokens import Doc
from tqdm import tqdm


class AugmentSentences(BaseStep):
    def __init__(self, augment, tokeniser):
        super().__init__()
        self.augment = augment
        self.tokeniser = tokeniser
        self.annotations = None

    def fit(self, data_inputs: None, expected_outputs: List[int]) -> '_FittableStep':
        self.annotations = expected_outputs
        return self

    def transform(self, sents: List[str], to_file: bool = False) -> (List[str], List[int]):
        if self.augment == 0:
            return sents, self.annotations
        augmented_sents = []
        for index, sent in enumerate(tqdm(sents, desc="Augmenting dataset")):
            new_sents = [self._generateNewSentence(sent) for i in range(self.augment)]
            new_sents = [sent for sent in new_sents if sent != ""]
            new_sents = list(set(new_sents))
            new_sents = [(new_sent, self.annotations[index]) for new_sent in new_sents]
            augmented_sents.append((sent, self.annotations[index]))
            augmented_sents += new_sents
        new_sents = [s[0] for s in augmented_sents]
        annotations = [s[1] for s in augmented_sents]
        new_sents += sents
        annotations += self.annotations
        return new_sents, annotations

    def inverse_transform(self, processed_outputs):
        pass

    def _generateNewSentence(self, sent: str, ) -> str:
        """
        returns a new sentence using PLSDA (Xiang et al. 2020)
        :param sentence: input sentence to draw substitute candidates from
        :return: new sentence
        """
        sentence = self.tokeniser(sent)
        sentence_tokens = [str(token) for token in sentence]
        sentence_spaces = [True if token.whitespace_ == ' ' else False for token in sentence]
        sentence_pos = [(w.text, w.pos_) for w in sentence]
        for index, (word, pos) in enumerate(sentence_pos):
            sentence_tokens[index] = self._substituteLexeme(word, pos)
        new_sentence = str(Doc(sentence.vocab, words=sentence_tokens, spaces=sentence_spaces))
        if new_sentence == str(sentence):
            return ""
        return new_sentence

    @staticmethod
    def _substituteLexeme(word, pos_tag):
        """
        Helper function that receives a word and returns
        a randomly selected synonym
        """
        if pos_tag == "NOUN":
            pos_ = nltk.corpus.wordnet.NOUN
        elif pos_tag == "ADJ":
            pos_ = nltk.corpus.wordnet.ADJ
        else:
            return word
        synsets = [synset.lemma_names('fra') for synset in nltk.corpus.wordnet.synsets(word, lang='fra', pos=pos_)]
        try:
            synsets = max(synsets)
        except ValueError:
            synsets = []
        if len(synsets) != 0:
            subs_word = choice(synsets)
        else:
            return word
        return subs_word
