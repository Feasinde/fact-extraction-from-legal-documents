from typing import List, Any
import gensim
import torch
from neuraxle.base import BaseTransformer
from tqdm import tqdm


def tensorFromSentence(sentence: str, tokeniser: Any, word2index: dict, max_len=100):
    sent = sentence[:max_len] if len(sentence) >= max_len else sentence
    sent = tokeniser(sent)
    index_list = []
    for word in sent:
        try:
            index = word2index[str(word).lower()]
        except KeyError:
            index = 0
        index_list.append(index)
    return torch.tensor(index_list, dtype=torch.long)


class VectoriseSentences(BaseTransformer):
    def __init__(self, emb_model: gensim.models.keyedvectors.Word2VecKeyedVectors, tokeniser, max_len: int = 100):
        super().__init__()
        self.emb_model = emb_model
        self.word2index = {token: token_index for token_index, token in enumerate(self.emb_model.index2word)}
        self.tokeniser = tokeniser
        self.max_len = max_len

    def transform(self, sents: List[str]) -> (List[torch.Tensor]):
        vectorised_sentences = [tensorFromSentence(sent, self.tokeniser, self.word2index, max_len=self.max_len) for sent in tqdm(sents, desc="Vectorising sentences")]
        return vectorised_sentences

    def inverse_transform(self, processed_outputs):
        pass
