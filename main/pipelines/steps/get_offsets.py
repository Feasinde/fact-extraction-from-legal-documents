from typing import Tuple, List, Dict

from neuraxle.base import BaseTransformer


def get_offsets(offsets: List[int]) -> Dict[str, int]:
    of = {
        "<-4": 0,
        "-4": 0,
        "-3": 0,
        "-2": 0,
        "-1": 0,
        "0": 0,
        "1": 0,
        "2": 0,
        "3": 0,
        "4": 0,
        ">4": 0}
    for n in offsets:
        if n == 0:
            of["0"] += 1
        elif n == 1:
            of["1"] += 1
        elif n == 2:
            of["2"] += 1
        elif n == 3:
            of["3"] += 1
        elif n == 4:
            of["4"] += 1
        elif n > 4:
            of[">4"] += 1
        elif n == -1:
            of["-1"] += 1
        elif n == -2:
            of["-2"] += 1
        elif n == -3:
            of["-3"] += 1
        elif n == -4:
            of["-4"] += 1
        elif n < -4:
            of["<-4"] += 1
    return of


class GetOffsets(BaseTransformer):
    def inverse_transform(self, processed_outputs):
        pass

    def transform(self, data_inputs: Tuple[List[List[int]], List[int]]) -> List[dict]:

        preds, expected = data_inputs
        offset_list = []
        for pred in preds:
            assert len(pred) == len(expected), "Predicted indices must have the same length as expected indices"
            offsets = [pred[i] - expected[i] for i in range(len(pred))]
            offsets = get_offsets(offsets)
            offset_list += [offsets]
        return offset_list
