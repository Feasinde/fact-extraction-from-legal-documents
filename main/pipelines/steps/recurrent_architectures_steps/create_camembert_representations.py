import pickle
from os import getcwd, mkdir
from os.path import expanduser, isdir
from typing import Any, List

import torch
import transformers
from neuraxle.base import BaseStep, _FittableStep
from tqdm import tqdm

from main.modules.pytorch_utils import get_device

DEVICE = get_device()
DATA_FOLDER = "{}/PycharmProjects/fact-extraction-from-legal-documents/data".format(expanduser("~"))

class CreateCamembertRepresentations(BaseStep):
    DATA_FOLDER = "{}/PycharmProjects/fact-extraction-from-legal-documents/data".format(expanduser("~"))

    def __init__(self, tokeniser: Any, model='camembert-base'):
        super().__init__()
        self.sentence_tokeniser = tokeniser
        self.tokeniser = transformers.AutoTokenizer.from_pretrained(model)
        self.model = transformers.AutoModel.from_pretrained(model)
        self.model.eval()
        self.model.to(DEVICE)

    def fit(self, data_inputs: None, expected_outputs: List[int]) -> 'CreateCamembertRepresentations':
        self.splitting_indices = expected_outputs
        return self

    def transform(self, data_inputs: List[str], data_loc: str = None, annotated=True) -> None:
        n_doc = 0
        if data_loc is None:
            camembert_representations_folder = "camembert_representations" if annotated else "camembert_representations_unannotated"
        else:
            camembert_representations_folder = data_loc
        for i, document in enumerate(tqdm(data_inputs, desc='Writing CamemBERT representations to disc')):
            document = self.sentence_tokeniser(document)
            filepath = f"{self.DATA_FOLDER}/{camembert_representations_folder}"
            if not isdir(filepath):
                mkdir(filepath)
            with open(f"{filepath}/{n_doc}.pickle", "wb") as doc:
                for sentence in document.sents:
                    sentence = str(sentence)
                    sentence = self.tokeniser.encode(sentence)
                    sentence = torch.tensor([sentence])
                    sentence = self.model(sentence.to(DEVICE))
                    sentence = sentence[1]
                    sentence = sentence.squeeze()
                    pickle.dump(sentence.cpu(), doc)
                if annotated:
                    annotations = torch.ones([self.splitting_indices[i]])
                    padding = len(list(document.sents)) - len(annotations)
                    annotations = torch.nn.functional.pad(annotations, pad=(0, padding), value=0)
                    pickle.dump(annotations, doc)
            n_doc += 1

    def inverse_transform(self, processed_outputs):
        pass
