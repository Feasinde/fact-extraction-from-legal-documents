import os
import pickle

import torch
from neuraxle.base import BaseStep
from neuraxle.hyperparams.space import HyperparameterSamples
from torch.utils.data import Dataset, DataLoader

from main.modules.pytorch_utils import get_device
from main.modules.recurrent_architecture_modules import LSTMLinear, GRULinear, AttentionEncoderDecoder
from main.pipelines.steps.neural_training_mixin import NeuralTrainingMixin

device = get_device()


class DocumentDataset(Dataset):
    def __init__(self, ds_loc: str, ds_len: int = None, annotated: bool = True, max_len: int = None):
        super(DocumentDataset, self).__init__()
        self.ds_loc = ds_loc
        self.ds_len = ds_len
        self.annotated = annotated
        self.max_len = max_len

    def __len__(self):
        if self.ds_len is not None:
            return self.ds_len
        return len(os.listdir(self.ds_loc))

    def __getitem__(self, item):
        if self.ds_len is not None and item >= self.ds_len:
            raise IndexError("Index out of range")
        filename = "{}/{}.pickle".format(self.ds_loc, str(item))
        doc = []
        with open(filename, "rb") as file:
            while True:
                try:
                    doc.append(pickle.load(file))
                except EOFError:
                    break
        if self.annotated is False:
            return torch.stack(doc)
        sentences = doc[:-1]
        sentences = torch.stack(sentences)
        annotations = doc[-1]
        if self.max_len is not None:
            return sentences[:self.max_len], annotations[:self.max_len]
        return sentences, annotations


class BinariseDocuments(NeuralTrainingMixin, BaseStep):

    DEFAULT_PARAMS = HyperparameterSamples({
        'batch_size': 1,
        'n_epochs': 25,
        'learning_rate': 1e-3,
        'weight_decay': 1e-4,
        'clip': 0.2,
        'lr_step': [10, 13],
        'lr_gamma': 0.1,
    })

    MODELS = {
        'lstm': LSTMLinear(embedding_size=768),
        'gru': GRULinear(embedding_size=768),
        'attn': AttentionEncoderDecoder(hidden_size=768, max_len=256, device=get_device())
    }

    def __init__(self, model: str, augment: int, model_arq: str,  save_model: bool = False, max_len: int = None):
        super().__init__()
        self.set_hyperparams(self.DEFAULT_PARAMS)
        self.save_model = save_model
        self.augment = augment
        self.model = self.MODELS[model]
        self.device = get_device()
        self.model.to(self.device)
        self._isFit = False
        self.model_arq = model_arq
        self.max_len = max_len

    def fit(self, data_inputs: str, expected_outputs=None) -> 'BinariseDocuments':
        dataset = DocumentDataset(ds_loc=data_inputs, max_len=self.max_len)
        self._trainIterations(dataset=dataset, collate_fn=None, model_arq=self.model_arq)
        if self.save_model:
            torch.save(self.model.state_dict(), "model.pth")
        self._isFit = True
        return self

    def transform(self, data_inputs):
        assert self._isFit, "Cannot transform data without fitting first"
        dataset = DocumentDataset(ds_loc=data_inputs, annotated=False)
        dataset = DataLoader(dataset, batch_size=self.hyperparams["batch_size"])
        output = []
        for datum in dataset:
            output += [self.model(datum)]
        return output

    def inverse_transform(self, processed_outputs):
        pass




