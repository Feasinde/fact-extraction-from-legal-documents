from typing import List, Any

import spacy
from neuraxle.base import BaseStep
from tqdm import tqdm

from main.pipelines.steps.sentence_classifier_steps.augment_sentences import AugmentSentences


class AugmentDocuments(AugmentSentences):
    def __init__(self, augment: int, tokeniser: Any):
        super().__init__(augment, tokeniser)
        self.augment = augment
        self.tokeniser = tokeniser
        self.splitting_indices = None

    def fit(self, data_inputs:None, expected_outputs: List[int]):
        self.splitting_indices = expected_outputs
        return self

    def transform(self, data_inputs: List[str], to_file: bool = False) -> (List[str], List[int]):
        if self.augment == 0:
            return data_inputs, self.splitting_indices
        new_documents = []
        new_indices = []
        augment_iteration = 0
        while augment_iteration < self.augment:
            for idx, document in enumerate(tqdm(data_inputs, desc="Augmenting dataset")):
                document = self.tokeniser(document)
                document = [str(sent) for sent in document.sents]
                document = [self._generateNewSentence(sent) for sent in document]
                document = " ".join(document)
                splitting_index = self.splitting_indices[idx]
                new_documents += [document]
                new_indices += [splitting_index]
            augment_iteration += 1
        new_documents += data_inputs
        new_indices += self.splitting_indices
        return new_documents, new_indices

