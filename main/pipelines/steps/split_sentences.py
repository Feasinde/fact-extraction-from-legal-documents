from typing import List, Any

import gensim
import torch
from neuraxle.base import BaseTransformer
from torch.utils.data import DataLoader
from tqdm import tqdm

from main.modules.pytorch_utils import get_device
from main.pipelines.steps.recurrent_architectures_steps.binarise_documents import DocumentDataset
from main.pipelines.steps.recurrent_architectures_steps.create_camembert_representations import DATA_FOLDER
from main.pipelines.steps.split_util import getBestSplit
from main.pipelines.steps.sentence_classifier_steps.classify_sentences import SentenceClassifier, \
    pad_sequence_unlabelled
from main.pipelines.steps.sentence_classifier_steps.vectorise_sentences import tensorFromSentence


class SplitSentences(BaseTransformer):
    def __init__(self,
                 model: SentenceClassifier,
                 tokeniser: Any,
                 emb_model: gensim.models.keyedvectors.Word2VecKeyedVectors,
                 gamma: List[float]):
        super().__init__()
        self.device = get_device()
        self.model = model
        self.model.eval()
        self.model.to(self.device)
        self.tokeniser = tokeniser
        self.word2index = {token: token_index for token_index, token in enumerate(emb_model.index2word)}
        self.gamma = gamma

    def transform(self, data_inputs: List[str]) -> List[List[int]]:
        output = [self.tokeniser(doc) for doc in tqdm(data_inputs, desc="Tokenising sentences for splitting")]
        output = [[tensorFromSentence(
            sent.text, tokeniser=self.tokeniser, word2index=self.word2index
        ) for sent in doc.sents] for doc in tqdm(output, desc="Vectorising sentences for splitting")]
        output = [DataLoader(doc, batch_size=512, collate_fn=pad_sequence_unlabelled) for doc in output]
        with torch.no_grad():
            output = [[self.model(sents.to(self.device), lengths) for sents, lengths in doc] for doc in tqdm(output, desc="Classifying sentences")]
            output = [doc[0] for doc in output]
            output = [[torch.squeeze(sent).item() for sent in doc] for doc in output]
        output = [[round(x) for x in doc] for doc in output]
        output = [[getBestSplit(doc, gamma=gamma) for doc in tqdm(output, desc="Retrieving splitting indices")] for gamma in tqdm(self.gamma)]
        return output

    def inverse_transform(self, processed_outputs):
        pass


class SplitDocuments(BaseTransformer):

    def __init__(self, model: torch.nn.Module, gamma: List[float], tokeniser: Any):
        super(SplitDocuments, self).__init__()
        self.device = get_device()
        self.model = model
        self.model.eval()
        self.model.to(self.device)
        self.gamma = gamma
        self.tokeniser = tokeniser

    def transform(self, data_inputs: str = None) -> List[List[int]]:
        if data_inputs is not None:
            ds_loc = data_inputs
        else:
            ds_loc = DATA_FOLDER+"/camembert_representations_unannotated"
        dataset = DocumentDataset(ds_loc=ds_loc, annotated=True)
        output = [self.model(dataset[i][0].unsqueeze(0).to(self.device)) for i in tqdm(range(len(dataset)), desc="Binarising documents")]
        output = [torch.round(doc) for doc in output]
        output = [[getBestSplit(doc, gamma=gamma) for doc in tqdm(output, desc="Retrieving splitting indices")] for
                  gamma in tqdm(self.gamma)]
        return output

    def inverse_transform(self, processed_outputs):
        pass
