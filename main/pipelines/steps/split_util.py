from typing import List


def getBestSplit(input_list: List[int], gamma) -> int:
    """
    returns best split between facts and analysis sections
    in : list of integers for candidate indices such as [1,1,0,1,0] for instance
    out : integer, position of the last paragraph in be included in the fact section
    """
    purities = {}
    for i in range(1, len(input_list) + 1):
        substring = input_list[:i]
        purity = sum(substring) / len(substring)
        purities[len(substring)] = purity
    purities = {key: value for key, value in purities.items() if value != 1}
    try:
        index = max(purities, key=lambda key: purities[key])
    except ValueError:
        print("ValueError! Skipping...")
        return len(input_list) // 2
    index = int(index * gamma)
    return int(sum(input_list[:index]))