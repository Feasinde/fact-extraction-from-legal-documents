import torch

from main.modules.pytorch_utils import get_device
from main.modules.recurrent_architecture_modules import AttentionEncoder, AttentionDecoder, AttentionEncoderDecoder, \
    GRULinear, LSTMLinear

VECTOR_SIZE = 768

TEST_INPUT = torch.rand(1, 15, VECTOR_SIZE)
TEST_HIDDEN = torch.rand(1, 1, VECTOR_SIZE)
EXPECTED_SIZE = torch.Size([TEST_INPUT.shape[1]])


def test_attn_encoder():
    encoder = AttentionEncoder(hidden_size=VECTOR_SIZE)
    output = encoder(TEST_INPUT)
    seq_len = TEST_INPUT.shape[1]
    assert output.shape == torch.Size([1, seq_len, VECTOR_SIZE])
    assert isinstance(output, torch.Tensor)


def test_attn_decoder():
    decoder = AttentionDecoder(hidden_size=768)
    decoder_output, hidden, sent_output = decoder(TEST_INPUT, TEST_HIDDEN, TEST_HIDDEN)
    assert decoder_output.shape[-1] == VECTOR_SIZE
    assert len(decoder_output.shape) == 3
    assert hidden.shape == torch.Size([1, 1, VECTOR_SIZE])
    assert sent_output.shape == torch.Size([])


def test_attn_encoder_decoder():
    model = AttentionEncoderDecoder(hidden_size=VECTOR_SIZE, max_len=128, device=get_device())
    output = model(TEST_INPUT.to(get_device()))
    assert output.shape == EXPECTED_SIZE


def test_GRULinear():
    model = GRULinear(embedding_size=VECTOR_SIZE)
    output = model(TEST_INPUT)
    assert output.shape == EXPECTED_SIZE


def test_LSTMLinear():
    model = LSTMLinear(embedding_size=VECTOR_SIZE)
    output = model(TEST_INPUT)
    assert  output.shape == EXPECTED_SIZE