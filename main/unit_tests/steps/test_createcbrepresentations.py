import pickle
from os.path import expanduser

import spacy

from main.pipelines.steps.recurrent_architectures_steps.create_camembert_representations import \
    CreateCamembertRepresentations
from main.unit_tests.test_data import TEST_DOCUMENTS, TEST_SPLITTING_INDICES

TOKENISER = spacy.load("fr_core_news_sm")


def test_create_camembert_representations_unannotated():
    step = CreateCamembertRepresentations(tokeniser=TOKENISER)
    step.transform(data_inputs=TEST_DOCUMENTS, annotated=False)
    for i in range(len(TEST_DOCUMENTS)):
        d = []
        loc = "{}/PycharmProjects/fact-extraction-from-legal-documents/data/camembert_representations_unannotated/{}.pickle".format(expanduser("~"), i)
        with open(loc, "rb") as file:
            while True:
                try:
                    d += [pickle.load(file)]
                except EOFError:
                    break
        sent = d
        assert len(sent) == len(list(TOKENISER(TEST_DOCUMENTS[i]).sents))


def test_create_camembert_representations():
    step = CreateCamembertRepresentations(tokeniser=TOKENISER)
    step.fit_transform(data_inputs=TEST_DOCUMENTS, expected_outputs=TEST_SPLITTING_INDICES)
    for i in range(len(TEST_DOCUMENTS)):
        d = []
        loc = "{}/PycharmProjects/fact-extraction-from-legal-documents/data/camembert_representations/{}.pickle".format(expanduser("~"), i)
        with open(loc, "rb") as file:
            while True:
                try:
                    d += [pickle.load(file)]
                except EOFError:
                    break
        sent = d[:-1]
        annotation = d[-1]
        assert len(sent) == len(list(TOKENISER(TEST_DOCUMENTS[i]).sents))
        assert len(annotation) == len(sent)
        assert all((d.item() == 1 or d.item() == 0) for d in annotation)
