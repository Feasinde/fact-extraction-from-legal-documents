from random import randint
from typing import List

from main.pipelines.steps.get_offsets import GetOffsets

TEST_INDICES_PRED = [[randint(5, 10) for i in range(10)], [randint(5, 10) for i in range(10)], [randint(5, 10) for i in range(10)]]
TEST_INDICES_EXP = [randint(5, 10) for i in range(10)]


def test_getoffsets():
    step = GetOffsets()
    offset_list = step.transform((TEST_INDICES_PRED, TEST_INDICES_EXP))
    assert isinstance(offset_list, List)
    assert all(isinstance(offsets, dict) for offsets in offset_list)
    assert all([isinstance(offsets[key], int) for key in offsets] for offsets in offset_list)
