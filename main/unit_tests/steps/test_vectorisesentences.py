import gensim
import spacy
import torch

from main.pipelines.steps.sentence_classifier_steps.vectorise_sentences import VectoriseSentences
from main.unit_tests.test_data import TEST_TAGGED_SENTENCES

emb_model = gensim.models.KeyedVectors.load_word2vec_format(
    "~/PycharmProjects/fact-extraction-from-legal-documents/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore'
)
TOKENISER = spacy.load("fr_core_news_sm")
TEST_SENTENCES = [sent[0] for sent in TEST_TAGGED_SENTENCES]


def test_vectorisesentences():
    step = VectoriseSentences(emb_model=emb_model, tokeniser=TOKENISER)
    assert isinstance(step, VectoriseSentences)
    sents = step.transform(TEST_SENTENCES)
    assert all(isinstance(sent, torch.Tensor) for sent in sents)
    assert all(len(sent) < step.max_len for sent in sents)
