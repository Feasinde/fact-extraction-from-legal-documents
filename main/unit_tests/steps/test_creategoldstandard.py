import pandas

from main.pipelines.steps.create_goldstandard import CreateGoldStandard
from main.unit_tests.test_data import TEST_SENTENCES_FOR_DATAFRAME

DF = TEST_DF = pandas.DataFrame(TEST_SENTENCES_FOR_DATAFRAME, columns=['Facts', 'Analyses'])

def test_creategoldstandard():
    step = CreateGoldStandard()
    output = step.transform(DF)
    assert all(isinstance(doc, str) for doc in output)
    print(output)