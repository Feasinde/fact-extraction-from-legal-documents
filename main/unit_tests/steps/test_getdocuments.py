import pandas
import spacy

from main.pipelines.steps.recurrent_architectures_steps.get_documents import GetDocuments
from main.unit_tests.test_data import TEST_SENTENCES_FOR_DATAFRAME

TOKENISER = spacy.load('fr_core_news_sm')
TEST_DF = pandas.DataFrame(TEST_SENTENCES_FOR_DATAFRAME, columns=['Facts', 'Analyses'])

def test_getdocuments():
    step = GetDocuments(tokeniser=TOKENISER)
    documents, splitting_indices = step.transform(data_inputs=TEST_DF)
    assert all(isinstance(document, str) for document in documents)
    assert all(isinstance(index, int) for index in splitting_indices)
