import os

import pytest
import spacy
import torch

from main.pipelines.steps.recurrent_architectures_steps.binarise_documents import DocumentDataset, BinariseDocuments
from main.unit_tests.test_data import TEST_DOCUMENTS

HOME_FOLDER = os.path.expanduser("~")
DS_LOC = "{}/PycharmProjects/fact-extraction-from-legal-documents/data/camembert_representations".format(HOME_FOLDER)
DS_LOC_UNANNOTATED = "{}/PycharmProjects/fact-extraction-from-legal-documents/data/camembert_representations_unannotated".format(HOME_FOLDER)
tokeniser = spacy.load("fr_core_news_sm")
lengths = [len(list(doc.sents)) for doc in [tokeniser(document) for document in TEST_DOCUMENTS]]


def test_documentdataset_unannotated():
    dataset = DocumentDataset(ds_loc=DS_LOC_UNANNOTATED, annotated=False)
    sentences = [dataset[i] for i in range(len(dataset))]
    assert all(sentences[i].shape == torch.Size([lengths[i], 768]) for i in range(len(dataset)))


def test_documentdataset():
    dataset = DocumentDataset(ds_loc=DS_LOC)
    sentences = [dataset[i][0] for i in range(len(dataset))]
    annotations = [dataset[i][1] for i in range(len(dataset))]
    assert all(sentences[i].shape == torch.Size([lengths[i], 768]) for i in range(len(dataset)))
    assert all(annotations[i].shape  == torch.Size([lengths[i]]) for i in range(len(dataset)))


@pytest.mark.parametrize("model", ["lstm", "gru", "attn"])
def test_fit(model):
    step = BinariseDocuments(model=model, augment=0, save_model=False)
    step.hyperparams['n_epochs'] = 1
    step = step.fit(data_inputs=DS_LOC)
    assert isinstance(step, BinariseDocuments)

@pytest.mark.parametrize("model", ["lstm", "gru", "attn"])
def test_transform(model):
    step = BinariseDocuments(model=model, augment=0, save_model=False)
    step._isFit = True
    output = step.transform(data_inputs=DS_LOC_UNANNOTATED)

