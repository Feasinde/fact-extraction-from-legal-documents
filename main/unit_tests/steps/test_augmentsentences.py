from main.pipelines.steps.sentence_classifier_steps.augment_sentences import AugmentSentences
from main.unit_tests.test_data import TEST_TAGGED_SENTENCES

import spacy

TOKENISER = spacy.load("fr_core_news_sm")
SENTS = [sent[0] for sent in TEST_TAGGED_SENTENCES]
ANNOTATIONS = [sent[1] for sent in TEST_TAGGED_SENTENCES]

def test_augmentsentences():
    augment = 4
    step = AugmentSentences(augment=augment, tokeniser=TOKENISER)
    _, (sents, annotations) = step.fit_transform(data_inputs=SENTS, expected_outputs=ANNOTATIONS)
    assert len(sents) <= augment * len(TEST_TAGGED_SENTENCES) + len(TEST_TAGGED_SENTENCES)
    assert all(isinstance(sent, str) for sent in sents)
    assert all(isinstance(annotation, int) for annotation in annotations)
    augment = 0
    step = AugmentSentences(augment=augment, tokeniser=TOKENISER)
    _, (sents, _) = step.fit_transform(data_inputs=SENTS, expected_outputs=ANNOTATIONS)
    assert len(sents) == len(SENTS)

