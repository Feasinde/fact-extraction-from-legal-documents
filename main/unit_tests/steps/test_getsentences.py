import pandas
import spacy
from main.pipelines.steps.sentence_classifier_steps.get_sentences import GetSentences
from main.unit_tests.test_data import TEST_SENTENCES_FOR_DATAFRAME


TEST_DF = pandas.DataFrame(TEST_SENTENCES_FOR_DATAFRAME, columns=['Facts', 'Analyses'])
TOKENISER = spacy.load("fr_core_news_sm")


def test_classifysentence():
    step = GetSentences(tokeniser=TOKENISER)
    assert isinstance(step, GetSentences)
    (sents, annotations), splitting_indices = step.transform(TEST_DF, to_file=False)
    assert all(isinstance(sent, str) for sent in sents)
    assert all(isinstance(annotation, int) for annotation in annotations)
    assert all(isinstance(index, int) for index in splitting_indices)

