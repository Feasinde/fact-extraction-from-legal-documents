import pandas
import spacy

from main.pipelines.steps.recurrent_architectures_steps.augment_documents import AugmentDocuments
from main.pipelines.steps.recurrent_architectures_steps.get_documents import GetDocuments
from main.pipelines.steps.sentence_classifier_steps.augment_sentences import AugmentSentences
from main.unit_tests.test_data import TEST_SENTENCES_FOR_DATAFRAME

TOKENISER = spacy.load('fr_core_news_sm')
TEST_DF = pandas.DataFrame(TEST_SENTENCES_FOR_DATAFRAME, columns=['Facts', 'Analyses'])

def test_augmentdocuments():
    augment = 2
    step = GetDocuments(tokeniser=TOKENISER)
    documents, splitting_indices = step.transform(data_inputs=TEST_DF)
    step = AugmentDocuments(augment=augment, tokeniser=TOKENISER)
    _, (documents, splitting_indices) = step.fit_transform(data_inputs=documents, expected_outputs=splitting_indices)
    assert all(isinstance(document, str) for document in documents)
    assert all(isinstance(index, int) for index in splitting_indices)