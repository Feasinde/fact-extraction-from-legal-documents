import torch

EMB_SIZE = 200


class Conv(torch.nn.Module):
    def __init__(self, max_len=50, kernel_size=3):
        super().__init__()
        self.max_len = max_len
        self.conv = torch.nn.Conv1d(in_channels=EMB_SIZE, out_channels=100, kernel_size=kernel_size)
        self.relu = torch.nn.ReLU()
        self.pool = torch.nn.MaxPool1d(self.max_len - kernel_size + 1)
        self.linear = torch.nn.Linear(in_features=100, out_features=1)

    def forward(self, x):
        output = torch.nn.functional.pad(x, pad=(0, self.max_len-x.shape[-1]))
        output = self.conv(output)
        output = self.relu(output)
        output = self.pool(output)
        output = output.permute(0, 2, 1)
        output = self.linear(output)
        return output.squeeze(-1)


class GRU(torch.nn.Module):
    def __init__(self,
                 embedding_size=200,
                 hidden_size=256,
                 dropout=0.5):
        super().__init__()
        self.gru = torch.nn.GRU(embedding_size,
                                hidden_size,
                                batch_first=True,
                                bidirectional=True,
                                num_layers=3,
                                dropout=dropout)

    def forward(self, x, lengths):
        output = torch.nn.utils.rnn.pack_padded_sequence(x, lengths, batch_first=True)
        _, output = self.gru(output)
        output = output[0]
        return output


class SentenceClassifier(torch.nn.Module):
    def __init__(self, embeddings_tensor, max_len=300):
        super().__init__()
        self.embedding = torch.nn.Embedding.from_pretrained(embeddings_tensor)
        self.gru = GRU()
        self.conv = Conv()
        self.linear = torch.nn.Linear(in_features=300, out_features=1)
        self.sigmoid = torch.nn.Sigmoid()
        self.max_len = max_len

    def forward(self, x, lengths):
        output = self.embedding(x)
        gru_output = self.gru(output, lengths)
        conv_output = self.conv(output.permute(0, 2, 1))
        output = torch.cat([gru_output, conv_output], dim=1)
        padding = self.max_len - len(output[0])
        output = torch.nn.functional.pad(output, pad=[0, padding])
        output = self.linear(output)
        output = self.sigmoid(output)
        return torch.squeeze(output)
