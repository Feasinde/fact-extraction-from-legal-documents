import torch


def get_device():
    is_cuda = torch.cuda.is_available()
    device = torch.device("cuda") if is_cuda else torch.device("cpu")
    print("cuda is available!" if is_cuda else "no cuda 4 u")
    return device