import argparse
import os

import torch

from main.pipelines.recurrent_architecture_pipelines import GetDocumentsPipeline, AugmentDocumentsPipeline, \
    CreateCamembertRepresentationsPipeline, RecurrentArchitecturePipeline, SplitDocumentsPipeline

parser = argparse.ArgumentParser()
parser.add_argument("--model", type=str)
parser.add_argument("--data_folder", type=str)
parser.add_argument("--augment", type=int)

args = parser.parse_args()

HOME_FOLDER = os.path.expanduser("~")
ds_loc = f"{HOME_FOLDER}/PycharmProjects/fact-extraction-from-legal-documents/data/{args.data_folder}"
recurrent_architecture_pipeline = RecurrentArchitecturePipeline(model=args.model, augment=args.augment, model_arq=args.model, max_len=256)
# recurrent_architecture_pipeline.steps['recurrent_architecture'].hyperparams['n_epochs'] = 1
recurrent_architecture_pipeline.fit(data_inputs=ds_loc)
model = recurrent_architecture_pipeline.steps["recurrent_architecture"].model
model.to(torch.device("cpu"))
torch.save(model.state_dict(), f"test_model_augm_{args.augment}_{args.model}")